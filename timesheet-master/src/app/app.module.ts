import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { EmployeeListComponent } from './employee/employee.component';
import { EmployeeService } from './services/employee.service';
import { EmployeetimesheetComponent } from './employeetimesheet/employeetimesheet.component';
import { AppRoutingModule } from './/app-routing.module';
import { AddtimesheetComponent } from './addtimesheet/addtimesheet.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    EmployeetimesheetComponent,
    AddtimesheetComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    EmployeeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
