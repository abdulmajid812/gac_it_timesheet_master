import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-employeetimesheet',
  templateUrl: './employeetimesheet.component.html',
  styleUrls: ['./employeetimesheet.component.scss']
})

export class EmployeetimesheetComponent implements OnInit {
  currentDateTime: Date;
  startWeekDate: Date;
  endWeekDate: Date;

  currentDateString: string;
  startDateString: string;
  endDateString: string;

  id: number;
  totalSunday: any;
  totalMonday: any;
  totalTuesday: any;
  totalWednesday: any;
  totalThursday: any;
  totalFriday: any;
  totalSaturday: any;

  employees: any;
  employeeTaskSheet: any;

  constructor(private employeeService: EmployeeService, private route: ActivatedRoute, private router: Router) {
    this.id = +this.route.snapshot.paramMap.get("id");

    this.currentDateTime = new Date();
    this.startWeekDate = new Date();
    this.endWeekDate = new Date();
    this.startWeekDate.setDate(this.currentDateTime.getDate() - this.currentDateTime.getDay());
    this.endWeekDate.setDate(this.startWeekDate.getDate() + 6);

    this.currentDateString = this.currentDateTime.toLocaleDateString();
    this.startDateString = this.startWeekDate.toLocaleDateString();
    this.endDateString = this.endWeekDate.toLocaleDateString();
  }


  ngOnInit() {
    this.totalSunday = 0;
    this.totalMonday = 0;
    this.totalTuesday = 0;
    this.totalWednesday = 0;
    this.totalThursday = 0;
    this.totalFriday = 0;
    this.totalSaturday = 0;

    this.employeeService.getallemployees("").subscribe(data => {
      this.employees = data;
    });

    this.loadEmployeeTimeSheet();

  }
  onEmployeeChange(val: any) {
    this.loadEmployeeTimeSheet();
  }
  onPreviousClick() {
    this.startWeekDate.setDate(this.startWeekDate.getDate() - 7);
    this.endWeekDate.setDate(this.startWeekDate.getDate() + 6);

    this.startDateString = this.startWeekDate.toLocaleDateString();
    this.endDateString = this.endWeekDate.toLocaleDateString();

    this.loadEmployeeTimeSheet();
  }
  onNextClick() {
    this.startWeekDate.setDate(this.startWeekDate.getDate() + 7);
    this.endWeekDate.setDate(this.startWeekDate.getDate() + 6);

    this.startDateString = this.startWeekDate.toLocaleDateString();
    this.endDateString = this.endWeekDate.toLocaleDateString();

    this.loadEmployeeTimeSheet();
  }
  onBack() {
    this.router.navigateByUrl('/employeelist');
  }
  onSave() {
    this.router.navigateByUrl('/addtimesheet/' + this.id);
  }
  loadEmployeeTimeSheet() {
    this.employeeService.getEmployeeTaskSheet(this.id, this.startDateString).subscribe(data => {
      this.employeeTaskSheet = data;

      this.totalSunday = 0;
      this.totalMonday = 0;
      this.totalTuesday = 0;
      this.totalWednesday = 0;
      this.totalThursday = 0;
      this.totalFriday = 0;
      this.totalSaturday = 0;

      for (let i = 0; i < this.employeeTaskSheet.length; i++) {
        this.totalSunday = this.totalSunday + (+this.employeeTaskSheet[i].sunday);
        this.totalMonday = this.totalMonday + (+this.employeeTaskSheet[i].monday);
        this.totalTuesday = this.totalTuesday + (+this.employeeTaskSheet[i].tuesday);
        this.totalWednesday = this.totalWednesday + (+this.employeeTaskSheet[i].wednesday);
        this.totalThursday = this.totalThursday + (+this.employeeTaskSheet[i].thursday);
        this.totalFriday = this.totalFriday + (+this.employeeTaskSheet[i].friday);
        this.totalSaturday = this.totalSaturday + (+this.employeeTaskSheet[i].saturday);
      }
    });
  }
}
