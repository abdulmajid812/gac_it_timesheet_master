import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EmployeeService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getallemployees(text: string) {
        return this.http.get(this.baseapi + "/employee/getall?text="+text);
    }
    getEmployeeTaskSheet(employeeid: number, startDate: string){
        return this.http.get(this.baseapi + "/employee/getemployeetimesheet?employeeid="+employeeid+"&start=" + startDate);
    }
    getalltasks(){
        return this.http.get(this.baseapi + "/employee/getalltasks");
    }
    savetimsheet(employeeid: number, taskid:number, workingdate:string, workinghours:string){
        return this.http.get(this.baseapi + "/employee/savetimesheet?employeeid="+employeeid+"&taskid="+taskid+"&workingdate="+workingdate+"&workinghours="+workinghours);
    }
}