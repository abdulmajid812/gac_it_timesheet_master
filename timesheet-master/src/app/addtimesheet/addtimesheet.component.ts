import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-addtimesheet',
  templateUrl: './addtimesheet.component.html',
  styleUrls: ['./addtimesheet.component.scss']
})
export class AddtimesheetComponent implements OnInit {
  id: number;
  tasks: any;

  employeeID: number;
  taskID: number;
  workingDate: string;
  workingHours: string;

  constructor(private employeeService: EmployeeService, private route: ActivatedRoute, private router: Router) 
  {

    this.id = +this.route.snapshot.paramMap.get("id");

    this.employeeID = this.id;
    this.taskID = 0;
    this.workingDate= "";
    this.workingHours="";


   }

  ngOnInit() {
    this.employeeService.getalltasks().subscribe(data => {
      this.tasks = data;
      this.taskID = +this.tasks[0].id;
    });
    
  }
  onSave(){
    this.employeeService.savetimsheet(this.employeeID,this.taskID,this.workingDate,this.workingHours).subscribe();
    this.router.navigateByUrl('/employeetimesheet/'+this.id);
  }
  onBack(){
    this.router.navigateByUrl('/employeetimesheet/'+this.id);
  }

}
