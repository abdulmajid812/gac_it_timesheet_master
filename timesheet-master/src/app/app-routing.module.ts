import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, RouterModule, Routes } from '@angular/router';
import { EmployeeListComponent } from './employee/employee.component';
import { EmployeetimesheetComponent } from './employeetimesheet/employeetimesheet.component';
import { AddtimesheetComponent } from './addtimesheet/addtimesheet.component';

const routes: Routes = [
  { path: '', redirectTo: '/employeelist', pathMatch: 'full' },
  { path: 'employeetimesheet/:id', component: EmployeetimesheetComponent },
  { path: 'employeelist', component: EmployeeListComponent },
  { path: 'addtimesheet/:id', component: AddtimesheetComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
